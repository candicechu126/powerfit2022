// Import the functions you need from the SDKs you need
import * as firebase from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyDWe3FI8O1bK4f8uvOvXUUQ7ALlauiGxY8",
  authDomain: "reactnativeloginsystem.firebaseapp.com",
  projectId: "reactnativeloginsystem",
  storageBucket: "reactnativeloginsystem.appspot.com",
  messagingSenderId: "111382970909",
  appId: "1:111382970909:web:8c182df0c07b33fe223cf4",
  measurementId: "G-HG07BWT5P3",
  databaseURL: "https://reactnativeloginsystem-default-rtdb.firebaseio.com/",
};

// Initialize Firebase
// app = firebase.initializeApp(firebaseConfig);

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

// const auth = firebase.auth();

export { firebase };
