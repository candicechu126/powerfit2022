import React from "react";
import { StyleSheet } from "react-native";
import { SafeAreaProvider } from "react-native-safe-area-context";
import IntroScreen from "./screen/IntroScreen";
import LoginScreen from "./screen/LoginScreen";
import SignupScreen from "./screen/SignupScreen";
import ForgotPassword from "./screen/ForgotPassword";
import Profile from "./screen/Profile";
import Index from "./screen/IndexScreen";
import SplashScreen from "./screen/SplashScreen";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import PoseDetector from "./screen/PoseDetector";
import WorkoutSelectPage from "./screen/WorkoutSelectPage";
import Home from "./screen/Home";
import EditProfile from "./screen/EditProfileScreen";
const Stack = createStackNavigator();
export default function App({ navigation }) {
  return (
    <NavigationContainer>
      <SafeAreaProvider>
        <Stack.Navigator
          screenOptions={{
            headerShown: false,
          }}
        >
          {/* <Stack.Screen name="Index" component={Index} /> */}
          <Stack.Screen name="SplashScreen" component={SplashScreen} />
          <Stack.Screen name="IntroScreen" component={IntroScreen} />
          <Stack.Screen name="LoginScreen" component={LoginScreen} />
          <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
          <Stack.Screen name="SignupScreen" component={SignupScreen} />
          <Stack.Screen name="Profile" component={Profile} />
          <Stack.Screen name="Index" component={Index} />
          <Stack.Screen name="PoseDetector" component={PoseDetector} />
          <Stack.Screen
            name="WorkoutSelectPage"
            component={WorkoutSelectPage}
          />
          <Stack.Screen name="Home" component={Home} />
          <Stack.Screen name="EditProfile" component={EditProfile} />
        </Stack.Navigator>
      </SafeAreaProvider>
    </NavigationContainer>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
