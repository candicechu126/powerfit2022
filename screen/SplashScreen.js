import { History } from "@tensorflow/tfjs-layers";
import React, { Component } from "react";
import { View, StatusBar, StyleSheet, ImageBackground } from "react-native";
import * as Animatble from "react-native-animatable";
// import { LinearGradient } from "expo-linear-gradient";

export default class SplashScreen extends Component {
  goToScreen(routeName) {
    this.props.navigation.navigate(routeName);
  }
  componentDidMount() {
    setTimeout(
      () => {
        this.goToScreen("IntroScreen");
      },
      5000,
      this
    );
  }
  render() {
    return (
      // <LinearGradient
      //   start={{ x: 0, y: 1 }}
      //   end={{ x: 0, y: 0 }}
      //   colors={["#90caf9", "#fff", "#90caf9"]}
      //   style={styles.linearGradient}
      // >
      <ImageBackground
        source={require("../assets/bg/bgImg.png")}
        resizeMode="cover"
        style={styles.image}
      >
        <View
          style={{
            flex: 1,
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <StatusBar translucent backgroundColor="rgba(0,0,0,2)" />
          <Animatble.Image
            animation="pulse"
            easing="ease-in-out"
            iterationCount="infinite"
            style={{ width: 240, height: 200, margin: 10 }}
            source={require("../assets/bg/splashScr.png")}
          />
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  // linearGradient: {
  //   flex: 1,
  // },
  image: {
    flex: 1,
    justifyContent: "center",
  },
});
