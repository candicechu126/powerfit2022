import React, { useState } from "react";
import {
  KeyboardAvoidingView,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  View,
  Text,
  ImageBackground,
  Alert,
  Dimensions,
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome5";
import { firebase } from "../firebase";
const window = Dimensions.get("window");
const screen = Dimensions.get("screen");

const SignupScreen = ({ navigation }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isInputEmail, setIsInputEmail] = useState(false);
  const [isInputPWD, setIsInputPWD] = useState(false);
  const [hidePass, setHidePass] = useState(true);
  const handleEmail = () => setIsInputEmail(true);
  const handleBlurEmail = () => setIsInputEmail(false);
  const handlePWD = () => setIsInputPWD(true);
  const handleBlurPWD = () => setIsInputPWD(false);
  const handleLogin = () => {
    navigation.navigate("LoginScreen");
  };
  // .catch((error) => {
  //   Alert.alert("Warning", "Fail sign up");
  // });
  const handleSignup = () => {
    firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then((userCredentials) => {
        const user = userCredentials.user;
        Alert.alert("Sign Up Successfully");
      })
      .catch((error) => alert(error.message));
  };

  // const checkNull = () => {
  //   email.length === 0 && password.length === 0
  //     ? Alert.alert("Invaild email/password")
  //     : { handleSignup };
  // };

  return (
    <View
      style={{
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <View
        style={{
          width: screen.width,
          backgroundColor: "#E6F0FA",
          flex: 1,
        }}
      />
      <View
        style={{
          backgroundColor: "#0466C8",
          width: screen.width,
          flex: 1,
        }}
      />

      <View
        style={{
          position: "absolute",
          justifyContent: "center",
          alignItems: "center",
          paddingTop: 30,
          backgroundColor: "#fff",
          borderRadius: 6,
          width: "85%",
          height: "60%",
        }}
      >
        <KeyboardAvoidingView behavior="padding">
          <View style={styles.container} level="1">
            <Text style={styles.signupTitle}>Sign Up</Text>
            <TextInput
              style={styles.input}
              onFocus={handleEmail}
              onBlur={handleBlurEmail}
              placeholderTextColor={isInputEmail ? "#C9E3EE" : "#7EB6DF"}
              placeholder="Email"
              value={email}
              autoCapitalize="none"
              onChangeText={(text) => setEmail(text)}
              clearButtonMode="while-editing"
            />
            <View>
              <TextInput
                style={styles.input}
                onFocus={handlePWD}
                onBlur={handleBlurPWD}
                placeholderTextColor={isInputPWD ? "#C9E3EE" : "#7EB6DF"}
                placeholder="Password"
                value={password}
                autoCapitalize="none"
                onChangeText={(text) => setPassword(text)}
                secureTextEntry={hidePass ? true : false}
                clearButtonMode="while-editing"
              />
              <View
                style={{
                  paddingTop: screen.width * 0.07,
                  width: screen.width * 0.7,
                  alignItems: "flex-end",
                  position: "absolute",
                }}
              >
                <Icon
                  name={hidePass ? "eye-slash" : "eye"}
                  size={15}
                  color="grey"
                  onPress={() => setHidePass(!hidePass)}
                />
              </View>
            </View>
            <View style={{ paddingTop: 30 }}>
              <TouchableOpacity
                onPress={handleSignup}
                style={styles.signUpButton}
              >
                <Text style={styles.loginText}>Sign Up</Text>
              </TouchableOpacity>
            </View>
            <View
              style={{
                color: "#616161",
                flexDirection: "row",
                paddingTop: 25,
              }}
            >
              <Text
                style={{
                  color: "#616161",
                  fontSize: 16,
                }}
              >
                Already have an account?
              </Text>
              <TouchableOpacity onPress={handleLogin}>
                <Text style={styles.signuptext}>Sign In</Text>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAvoidingView>
      </View>
    </View>
  );
};

export default SignupScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    margin: 20,
    alignItems: "center",
  },
  input: {
    margin: 15,
    width: screen.width * 0.75,
    padding: 12,
    fontSize: 16,
    alignSelf: "center",
    backgroundColor: "#fff",
    borderRadius: 5,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    borderRadius: 25,
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
  },
  tinyLogo: {
    width: 150,
    height: 150,
    marginBottom: 60,
    borderRadius: 100,
  },
  signUpButton: {
    width: screen.width * 0.75,
    backgroundColor: "#0466C8",
    maxHeight: 80,
    borderRadius: 25,
    padding: 10,
    alignItems: "center",
    // alignSelf: "center",
  },
  loginText: {
    color: "#fff",
    fontSize: 20,
    fontWeight: "bold",
  },
  signuptext: {
    color: "#0466C8",
    fontWeight: "bold",
    fontSize: 16,
  },
  signupTitle: {
    color: "#0466C8",
    fontWeight: "bold",
    fontSize: 32,
    paddingTop: screen.height * 0.02,
    paddingBottom: screen.height * 0.05,
  },
  image: {
    flex: 1,
    width: "100%",
    height: "100%",
  },
});
