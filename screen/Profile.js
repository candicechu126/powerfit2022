import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  Image,
  ScrollView,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Alert,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { firebase } from "../firebase";
// import * as firebase from "firebase";

// var config = {
//   projectId: "reactnativeloginsystem",
//   databaseURL: "https://reactnativeloginsystem-default-rtdb.firebaseio.com/",
// };

// if (!firebase.apps.length) {
//   firebase.initializeApp(config);
// }
export default function Profile({ navigation }) {
  // const navigation = useNavigation();
  const onPressHandler = () => navigation.navigate("EditProfile");
  const [getM, setGetM] = useState("");
  const [getN, setGetN] = useState("");

  useEffect(() => {
    firebase
      .database()
      .ref("users/email")
      .on("value", (item) => {
        setGetM(item.val());
      });
  });

  useEffect(() => {
    for (var i = 0; i < getM.length; i++) {
      getM.charAt(i) === "@" && setGetN(getM.substring(0, i));
    }
  });

  return (
    <SafeAreaView>
      <Image
        source={require("../assets/bg/fitness-bg.jpeg")}
        style={{ padding: 110, width: "100%", height: "20%" }}
      />
      <View style={{ alignItems: "center" }}>
        <Image
          source={require("../assets/bg/bgImg.png")}
          style={{
            width: 120,
            height: 120,
            borderRadius: 100,
            marginTop: -70,
            backgroundColor: "#fff",
          }}
        />
        <Text
          style={{
            fontWeight: "bold",
            padding: 5,
            fontSize: 25,
          }}
        >
          {getN}
        </Text>
        <Text
          style={{
            fontWeight: "bold",
            color: "#616161",
            fontSize: 16,
            // padding: 5,
          }}
        >
          {getM}
        </Text>
        <TouchableOpacity onPress={onPressHandler}>
          <Ionicons name="create" size={25} color="#616161"></Ionicons>
        </TouchableOpacity>
      </View>
      <ScrollView style={{ margin: 5, height: "50%" }}>
        <View style={styles.target}>
          <Ionicons name="nutrition-outline" size={30} color="#0466C8" />
          <Text style={styles.targetText}>Eat healthy</Text>
        </View>
        <View style={styles.target}>
          <Ionicons name="barbell-outline" size={30} color="#0466C8" />
          <Text style={styles.targetText}>Get regular exercise</Text>
        </View>
        <View style={styles.target}>
          <Ionicons name="water" size={30} color="#0466C8" />
          <Text style={styles.targetText}>Drink water</Text>
        </View>
        <View style={styles.target}>
          <Ionicons name="wine-outline" size={30} color="#0466C8" />
          <Text style={styles.targetText}>
            Limit how much alcohol you drink
          </Text>
        </View>
        <View style={styles.target}>
          <Ionicons name="happy" size={30} color="#0466C8" />
          <Text style={styles.targetText}>Be active and relax</Text>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  info: {
    flex: 1,
    paddingTop: 10,
    flexDirection: "column",
    backgroundColor: "#F0EDF6",
    justifyContent: "center",
    alignItems: "center",
    height: 250,
  },
  userImg: {
    height: 80,
    width: 80,
    borderRadius: 75,
  },
  userName: {
    flex: 1,
    fontSize: 18,

    fontWeight: "bold",
    marginTop: 10,
    marginBottom: 10,
  },
  separator: {
    marginVertical: 8,
    borderBottomColor: "#0466C8",
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  target: {
    alignSelf: "center",
    flexDirection: "row",
    justifyContent: "center",
    backgroundColor: "#fff",
    width: "90%",
    padding: 15,
    paddingBottom: 20,
    borderRadius: 10,
    shadowOpacity: 10,
    shadowColor: "#F0EDF6",
    elevation: 15,
    marginTop: 20,
  },
  targetText: {
    fontSize: 14,
    color: "#0466C8",
    fontWeight: "bold",
    marginLeft: 15,
    // justifyContent: "space-between",
    marginTop: 8,
  },
});
