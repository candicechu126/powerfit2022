import React, { useState } from "react";
import {
  KeyboardAvoidingView,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Dimensions,
  View,
  Text,
  Image,
  Alert,
  ImageBackground,
  SafeAreaView,
  Button,
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome5";
import { useNavigation } from "@react-navigation/native";
import { firebase } from "../firebase";

const window = Dimensions.get("window");
const screen = Dimensions.get("screen");

const LoginScreen = () => {
  const navigation = useNavigation();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isInputEmail, setIsInputEmail] = useState(false);
  const [isInputPWD, setIsInputPWD] = useState(false);
  const [hidePass, setHidePass] = useState(true);
  const handleEmail = () => setIsInputEmail(true);
  const handleBlurEmail = () => setIsInputEmail(false);
  const handlePWD = () => setIsInputPWD(true);
  const handleBlurPWD = () => setIsInputPWD(false);
  const handleSignup = () => navigation.navigate("SignupScreen");
  const goToForgotPassword = () => navigation.navigate("ForgotPassword");
  const [getM, setGetM] = useState("");
  const handleLogin = () => {
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then((userCredentials) => {
        const user = userCredentials.user;
        navigation.navigate("Index");
        setPassword("");
        firebase
          .database()
          .ref("users")
          .update({
            email,
          })
          .then((email) => {
            //success callback
            // console.log("data ", email);
          })
          .catch((error) => {
            //error callback
            console.log("error ", error);
          });
        firebase
          .database()
          .ref("users/user1")
          .push({
            email,
          })
          .then((email) => {
            //success callback
            // console.log("data ", email);
          })
          .catch((error) => {
            //error callback
            console.log("error ", error);
          });
        firebase
          .database()
          .ref("users/email")
          .on("value", (snapshot) => {
            setGetM(snapshot.val());
          });
      })
      .catch((error) => alert(error.message));
  };

  return (
    <ImageBackground
      source={require("../assets/bg/loginBg.png")}
      resizeMode="cover"
      style={styles.imageBg}
    >
      <SafeAreaView style={styles.container} level="1">
        <KeyboardAvoidingView behavior="padding">
          <Image
            style={styles.tinyLogo}
            source={require("../assets/logo.png")}
          />
          <TextInput
            style={styles.input}
            onFocus={handleEmail}
            onBlur={handleBlurEmail}
            placeholderTextColor={isInputEmail ? "#C9E3EE" : "#7EB6DF"}
            placeholder="Email"
            value={email}
            autoCapitalize="none"
            onChangeText={(text) => setEmail(text)}
            clearButtonMode="while-editing"
          />
          <View>
            <TextInput
              style={styles.input}
              onFocus={handlePWD}
              onBlur={handleBlurPWD}
              placeholderTextColor={isInputPWD ? "#C9E3EE" : "#7EB6DF"}
              placeholder="Password"
              value={password}
              autoCapitalize="none"
              onChangeText={(text) => setPassword(text)}
              secureTextEntry={hidePass ? true : false}
              clearButtonMode="while-editing"
            />
            <View
              style={{
                paddingTop: screen.width * 0.07,
                width: screen.width * 0.8,
                alignItems: "flex-end",
                position: "absolute",
              }}
            >
              <Icon
                name={hidePass ? "eye-slash" : "eye"}
                size={15}
                color="grey"
                onPress={() => setHidePass(!hidePass)}
              />
            </View>
          </View>
          <View style={{ paddingTop: 30, paddingBottom: 20 }}>
            <TouchableOpacity onPress={handleLogin} style={styles.loginButton}>
              <Text style={styles.loginText}>Login</Text>
            </TouchableOpacity>
          </View>

          <Button
            title="Forgot Password?"
            color="#0466C8"
            onPress={goToForgotPassword}
          />
          <View
            style={{
              color: "#616161",
              flexDirection: "row",
              justifyContent: "center",
              paddingTop: 5,
            }}
          >
            <Text
              style={{
                color: "#616161",
                fontSize: 14,
              }}
            >
              Dont't have an account yet?
            </Text>
            <TouchableOpacity onPress={handleSignup}>
              <Text style={styles.signuptext}>Sign Up</Text>
            </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>
      </SafeAreaView>
    </ImageBackground>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    padding: screen.height * 0.1,
    // backgroundColor: "#C9E3EE",
  },
  image: {
    flex: 1,
    width: "100%",
    height: "100%",
  },
  imageBg: {
    flex: 1,
    justifyContent: "center",
  },
  input: {
    margin: 15,
    width: screen.width * 0.8,
    padding: 12,
    fontSize: 16,
    alignSelf: "center",
    backgroundColor: "#fff",
    borderRadius: 25,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
  },
  tinyLogo: {
    width: 200,
    height: 160,
    marginBottom: 50,
    alignSelf: "center",
  },
  loginButton: {
    width: screen.width * 0.8,
    backgroundColor: "#0466C8",
    maxHeight: 80,
    borderRadius: 25,
    padding: 10,
    alignSelf: "center",
  },
  loginText: {
    color: "#fff",
    fontSize: 20,
    fontWeight: "bold",
    alignSelf: "center",
  },
  signuptext: {
    color: "#0466C8",
    fontWeight: "bold",
    fontSize: 16,
  },
});
