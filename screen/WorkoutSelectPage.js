import React, { useState } from "react";
import {
  Image,
  FlatList,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Alert,
  Modal,
  Dimensions,
} from "react-native";
import { SearchBar } from "react-native-elements";

const window = Dimensions.get("window");
const screen = Dimensions.get("screen");

const Item = ({ item, onPress, backgroundColor, textColor }) => (
  <TouchableOpacity onPress={onPress} style={[styles.item, backgroundColor]}>
    <Image style={styles.image} source={item.igurl} />
    <Text style={[styles.title, textColor]}>{item.title}</Text>
  </TouchableOpacity>
);

export default function WorkoutSelectPage({ navigation }) {
  const [searchInput, setSearchinput] = useState("");
  const [selectedId, setSelectedId] = useState(null);
  const [currentData, setCurrentData] = useState(data);
  // const [openCamera, setOpenCamera] = useState(null);

  const handleSearch = (val) => {
    setSearchinput(val);
    setCurrentData(
      data.filter((item) =>
        item.title.toLowerCase().includes(val.toLowerCase())
      )
    );
  };

  const renderItem = ({ item }) => {
    const backgroundColor = item.id === selectedId ? "#E6F0FA" : "#fff";
    const color = item.id === selectedId ? "#616161" : "#0466C8";
    const img = item.igurl;
    return (
      <Item
        item={item}
        onPress={() => {
          setSelectedId(item.id),
            Alert.alert(
              '"Fitness Partner" Would Like to Access the Camera',
              "This will let you take record video",
              [
                {
                  text: "Block",
                  onPress: () => {
                    console.log({ backgroundColor });
                  },
                  style: "cancel",
                },
                {
                  text: "Allow",
                  onPress: () => {
                    navigation.navigate("PoseDetector", { data: item.title });
                  },
                },
              ]
            );
        }}
        backgroundColor={{ backgroundColor }}
        ImageBackground={img}
        textColor={{ color }}
      />
    );
  };

  return (
    //    <Modal
    //       animationType="slide"
    //       // transparent={true}
    //       visible={openCamera}
    //       onRequestClose={() => {
    //         Alert.alert("Camera has been closed.");
    //         this.setModalVisible(!openCamera);
    //       }}
    //     >
    //       <View style={styles.buttonCloseCamera}>
    //         <MaterialCommunityIcons
    //           name="arrow-left"
    //           size={24}
    //           color="#7C4DFF"
    //           onPress={() => setOpenCamera(false)}
    //         />
    //       </View>
    //        <PoseDetector />
    //       <AccessCamera />
    //     </Modal>
    //  <Text style={styles.heading}>Fitness WorkOut</Text>

    <SafeAreaView style={styles.container}>
      <SearchBar
        lightTheme
        inputStyle={{ fontSize: 16 }}
        platform="ios"
        cancelButtonTitle="cancel"
        inputContainerStyle={{ backgroundColor: "#E6F0FA" }}
        value={searchInput}
        onChangeText={handleSearch}
        placeholder={"search workout name"}
      />
      <FlatList
        data={currentData}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
        extraData={selectedId}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    backgroundColor: "#fff",
  },
  heading: {
    fontSize: 26,
    fontWeight: "bold",
    marginTop: 12,
    marginLeft: 12,
  },
  textInput: {
    height: 39,
    width: "90%",
    backgroundColor: "#ECF3FF",
    borderRadius: 20,
    paddingLeft: 15,
    marginTop: 12,
    marginBottom: 12,
  },
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
    backgroundColor: "#fff",
  },
  item: {
    padding: 20,
    height: 250,
    marginVertical: 14,
    marginHorizontal: 14,
    borderRadius: 15,
    shadowColor: "#0E4666",
    shadowOffset: {
      width: 2,
      height: 3,
    },
    shadowOpacity: 0.5,
    shadowRadius: 3.5,
    elevation: 8,
  },
  image: {
    width: 250,
    height: 200,
    marginHorizontal: 60,
  },
  title: {
    fontSize: 18,
  },
  buttonCloseCamera: {
    paddingTop: window.height * 0.06,
    paddingLeft: window.width * 0.06,
    paddingBottom: window.width * 0.03,
    backgroundColor: "#0466C8",
    // alignItems: "center",
  },
});

const data = [
  {
    id: "001",
    title: "Jumping Jacks",
    igurl: {
      uri: "https://firebasestorage.googleapis.com/v0/b/reactnativeloginsystem.appspot.com/o/1-Jumping-jacks.png?alt=media&token=f0cece94-118a-4077-ab0a-10650905f350",
    },
  },
  {
    id: "002",
    title: "Reverse Lunge",
    igurl: {
      uri: "https://firebasestorage.googleapis.com/v0/b/reactnativeloginsystem.appspot.com/o/2-Reverse-Lunge.png?alt=media&token=044898ba-4335-40be-8290-5bd3c456f029",
    },
  },
  {
    id: "003",
    title: "Side Leg Raise",
    igurl: {
      uri: "https://firebasestorage.googleapis.com/v0/b/reactnativeloginsystem.appspot.com/o/3-Side-leg-raise.png?alt=media&token=12608520-39a6-4682-b617-b2d143dc574d",
    },
  },
  {
    id: "004",
    title: "Squat Exercise Illustration",
    igurl: {
      uri: "https://firebasestorage.googleapis.com/v0/b/reactnativeloginsystem.appspot.com/o/4-squat-exercise-illustration.png?alt=media&token=73a5e535-3f34-4f24-866e-8eef244cb928",
    },
  },
  {
    id: "005",
    title: "Side Lunge",
    igurl: {
      uri: "https://firebasestorage.googleapis.com/v0/b/reactnativeloginsystem.appspot.com/o/5-side-lunge-exercise-illustration.png?alt=media&token=e2d5518a-0aa9-4d53-9574-6b1a5570d1f2",
    },
  },
  {
    id: "006",
    title: "High Knees",
    igurl: {
      uri: "https://firebasestorage.googleapis.com/v0/b/reactnativeloginsystem.appspot.com/o/6-High-Knees.png?alt=media&token=aecf6472-7c6f-47c0-80d9-ad25ad7c1a4b",
    },
  },
  {
    id: "007",
    title: "Plank",
    igurl: {
      uri: "https://firebasestorage.googleapis.com/v0/b/reactnativeloginsystem.appspot.com/o/7-Plank.png?alt=media&token=7b2af8a5-8ba2-4650-9223-2b6815441461",
    },
  },
  {
    id: "008",
    title: "Russian Twist",
    igurl: {
      uri: "https://firebasestorage.googleapis.com/v0/b/reactnativeloginsystem.appspot.com/o/8-Russian-twist.png?alt=media&token=56eaf2ce-614b-4087-bcb7-27b710acf809",
    },
  },
  {
    id: "009",
    title: "Marching Glute Bridge",
    igurl: {
      uri: "https://firebasestorage.googleapis.com/v0/b/reactnativeloginsystem.appspot.com/o/9-Marching-glute-bridge.png?alt=media&token=ce409be8-4d7e-45c6-a5dc-1c0b5a468ae8",
    },
  },
  {
    id: "010",
    title: "Lying Windshield Wipers",
    igurl: {
      uri: "https://firebasestorage.googleapis.com/v0/b/reactnativeloginsystem.appspot.com/o/10-Lying-Windshield-Wipers.png?alt=media&token=407067b9-5ea2-4ea6-a890-3d55975a64f4",
    },
  },
  {
    id: "011",
    title: "Curtsy Lunge",
    igurl: {
      uri: "https://firebasestorage.googleapis.com/v0/b/reactnativeloginsystem.appspot.com/o/11-curtsy-lunge-exercise-illustration.png?alt=media&token=13041af2-46c3-42eb-9060-1abe4fe66923",
    },
  },
  {
    id: "012",
    title: "Sumo Squat",
    igurl: {
      uri: "https://firebasestorage.googleapis.com/v0/b/reactnativeloginsystem.appspot.com/o/12-sumo-squat-exercise-illustration.png?alt=media&token=7b91f2ef-42d0-4c8c-ba6a-9585b4373aae",
    },
  },
  {
    id: "013",
    title: "Donkey Kick Pulses",
    igurl: {
      uri: "https://firebasestorage.googleapis.com/v0/b/reactnativeloginsystem.appspot.com/o/13-Donkey-Kick-pulses.png?alt=media&token=03cedcff-b1cb-478e-b5cb-116e65bd128b",
    },
  },
  {
    id: "014",
    title: "Bicycle Crunches",
    igurl: {
      uri: "https://firebasestorage.googleapis.com/v0/b/reactnativeloginsystem.appspot.com/o/14-bicycle-crunches-exercise-illustration.png?alt=media&token=e59ae5a2-5eae-451d-a055-6c99b88eeeea",
    },
  },
];
