import React, { useState } from "react";
import {
  StyleSheet,
  View,
  Text,
  Image,
  Dimensions,
  Modal,
  Button,
  SafeAreaView,
} from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import AppIntroSlider from "react-native-app-intro-slider";
import { EvilIcons } from "@expo/vector-icons";
import LoginScreen from "./LoginScreen";

const window = Dimensions.get("window");
const screen = Dimensions.get("screen");

export default function IntroScreen({ navigation }) {
  const [showRealApp, setShowRealApp] = useState(false);
  const [open, setOpen] = useState(true);
  const handleOpen = () => {
    setOpen(false);
  };
  const onDone = () => {
    setShowRealApp(true);
  };
  // const onSkip = () => {
  //   setShowRealApp(true);
  // };
  const renderNextButton = () => {
    return (
      <EvilIcons
        name="arrow-right"
        size={40}
        color="#03045e"
        style={{ marginRight: 30 }}
      />
    );
  };

  const renderDoneButton = () => {
    return (
      <EvilIcons
        name="check"
        size={40}
        color="#fff"
        style={{ marginRight: 30 }}
      />
    );
  };
  const renderItem = ({ item }) => {
    return (
      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 0, y: 1 }}
        colors={["#fff", "#0466c8", "#fff", "#fff"]}
        style={styles.linearGradient}
      >
        <View
          style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "space-around",
            paddingBottom: 100,
          }}
        >
          <Text style={styles.introTitleStyle}>{item.title}</Text>
          <Image style={styles.introImageStyle} source={item.image} />
          <Text style={styles.introTextStyle}>{item.text}</Text>
        </View>
      </LinearGradient>
    );
  };

  return (
    <>
      {showRealApp ? (
        <LoginScreen />
      ) : (
        <AppIntroSlider
          data={slides}
          renderItem={renderItem}
          renderNextButton={renderNextButton}
          onDone={onDone}
          renderDoneButton={renderDoneButton}
          // showSkipButton={true}
          // onSkip={onSkip}
        />
      )}
    </>
  );
}

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    backgroundColor: "#fff",
  },
  linearGradient: {
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 5,
    width: "100%",
    height: "100%",
  },
  introImageStyle: {
    width: 300,
    height: 200,
  },
  introTextStyle: {
    fontSize: 18,
    fontFamily: "Cochin",
    color: "black",
    textAlign: "center",
    paddingVertical: 30,
  },
  introTitleStyle: {
    fontSize: 25,
    fontFamily: "Cochin",
    color: "black",
    textAlign: "center",
    marginBottom: 16,
    fontWeight: "bold",
  },
  welbackText: {
    fontSize: 28,
    fontFamily: "Cochin",
    color: "#3c096c",
    textAlign: "center",
    marginBottom: window.height * 0.04,
    fontWeight: "bold",
  },
});

const slides = [
  {
    key: "s1",
    text: "Action is the key to all success",
    title: "Fitness Anywhere",
    image: require("../assets/component/welcomeImage1.png"),
  },
  {
    key: "s2",
    title: "Healthy Life",
    text: "Exercise is labor without weariness",
    // image: {
    //   uri: "https://firebasestorage.googleapis.com/v0/b/hardy-palace-218707.appspot.com/o/welcomeImage2.JPG?alt=media&token=aa709bd6-0156-420d-8f3f-9774ff3e0715",
    // },
    image: require("../assets/component/welcomeImage2.png"),
  },
  {
    key: "s3",
    title: "Save Time",
    text: "Tough times don’t last, Tough people do",
    // image: {
    //   uri: "https://firebasestorage.googleapis.com/v0/b/hardy-palace-218707.appspot.com/o/welcomeImage3.JPG?alt=media&token=c8a679fe-cfc9-4d1a-8c07-ae1c295027bf",
    // },
    image: require("../assets/component/welcomeImage3.png"),
  },
  {
    key: "s4",
    title: "Best Records",
    text: " Our bodies are our gardenss",
    // image: {
    //   uri: "https://firebasestorage.googleapis.com/v0/b/hardy-palace-218707.appspot.com/o/welcomeImage4.JPG?alt=media&token=9ead16e4-e28d-4e02-9e6a-836e293abba4",
    // },
    image: require("../assets/component/welcomeImage4.png"),
  },
  {
    key: "s5",
    title: "Reduce Injury",
    text: "No pain, no gain",
    // image: {
    //   uri: "https://firebasestorage.googleapis.com/v0/b/hardy-palace-218707.appspot.com/o/welcomeImage5.JPG?alt=media&token=4c99f1d2-ccde-4735-9107-c6ff8e67beb4",
    // },
    image: require("../assets/component/welcomeImage5.png"),
  },
  {
    key: "s6",
    title: "Easy to Learn",
    text: "Sweat is fat crying",
    // image: {
    //   uri: "https://firebasestorage.googleapis.com/v0/b/hardy-palace-218707.appspot.com/o/welcomeImage6.JPG?alt=media&token=8e0bdcec-ccaf-4591-881b-63edf96ac9e9",
    // },
    image: require("../assets/component/welcomeImage6.png"),
  },
];
