import React, { useState, useEffect } from "react";
import {
  createDrawerNavigator,
  DrawerContentScrollView,
} from "@react-navigation/drawer";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import {
  NativeBaseProvider,
  Box,
  Pressable,
  VStack,
  Text,
  HStack,
  Divider,
  Icon,
} from "native-base";
import Home from "./Home";
import About from "./About";
import Setting from "./EditPart/Setting";
import LoginScreen from "./LoginScreen";
import { firebase } from "../firebase";
// import * as firebase from "firebase";
// var config = {
//   projectId: "reactnativeloginsystem",
//   databaseURL: "https://reactnativeloginsystem-default-rtdb.firebaseio.com/",
// };

// if (!firebase.apps.length) {
//   firebase.initializeApp(config);
// }
const Drawer = createDrawerNavigator();

const getIcon = (screenName) => {
  switch (screenName) {
    case "Home":
      return "home";
    case "About":
      return "cellphone-information";
    case "Setting":
      return "cog-outline";
    case "Logout":
      return "logout";
    default:
      return undefined;
  }
};

const IndexScreen = (data) => {
  const [getM, setGetM] = useState("");
  const [getN, setGetN] = useState("");

  useEffect(() => {
    firebase
      .database()
      .ref("users/email")
      .on("value", (item) => {
        setGetM(item.val());
      });
  });

  useEffect(() => {
    for (var i = 0; i < getM.length; i++) {
      getM.charAt(i) === "@" && setGetN(getM.substring(0, i));
    }
  });

  return (
    <NativeBaseProvider>
      <Drawer.Navigator
        screenOptions={{
          drawerStyle: {
            backgroundColor: "#fff",
          },
        }}
        drawerContent={(props) => (
          <DrawerContentScrollView {...props} safeArea>
            <VStack space="6" my="2" mx="1">
              <Box px="4">
                <Text bold color="#616161" fontSize="18">
                  {getN}
                </Text>
                <Text fontSize="14" color="#616161">
                  {getM}
                </Text>
              </Box>
              <VStack divider={<Divider />} space="4">
                <VStack space="3">
                  {props.state.routeNames.map((name, index) => (
                    <Pressable
                      px="5"
                      py="3"
                      rounded="md"
                      bg={
                        index === props.state.index ? "#0466C8" : "transparent"
                      }
                      onPress={(event) => {
                        name === "Logout"
                          ? props.navigation.navigate("LoginScreen")
                          : props.navigation.navigate(name);
                      }}
                    >
                      <HStack space="7" alignItems="center">
                        <Icon
                          color={
                            index === props.state.index ? "#fff" : "#616161"
                          }
                          size="5"
                          as={<MaterialCommunityIcons name={getIcon(name)} />}
                        />
                        <Text
                          fontWeight="500"
                          color={
                            index === props.state.index ? "#fff" : "#616161"
                          }
                        >
                          {name}
                        </Text>
                      </HStack>
                    </Pressable>
                  ))}
                </VStack>
              </VStack>
            </VStack>
          </DrawerContentScrollView>
        )}
      >
        <Drawer.Screen name="Home" component={Home} />
        <Drawer.Screen name="About" component={About} />
        <Drawer.Screen name="Setting" component={Setting} />
        <Drawer.Screen
          name="Logout"
          component={LoginScreen}
          options={{ headerShown: false }}
        />
      </Drawer.Navigator>
    </NativeBaseProvider>
  );
};
export default IndexScreen;
