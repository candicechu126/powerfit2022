import React, { useEffect, useState, useRef } from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  Platform,
  useWindowDimensions,
} from "react-native";

import { Camera } from "expo-camera";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import * as tf from "@tensorflow/tfjs";
import * as posedetection from "@tensorflow-models/pose-detection";
// import * as ScreenOrientation from "expo-screen-orientation";
import {
  bundleResourceIO,
  cameraWithTensors,
} from "@tensorflow/tfjs-react-native";
import Svg, { Circle } from "react-native-svg";
import { ExpoWebGLRenderingContext } from "expo-gl";
import { CameraType } from "expo-camera/build/Camera.types";
const window = Dimensions.get("window");
const screen = Dimensions.get("screen");
// tslint:disable-next-line: variable-name
const TensorCamera = cameraWithTensors(Camera);

const IS_ANDROID = Platform.OS === "android";
const IS_IOS = Platform.OS === "ios";

// Camera preview size.
//
// From experiments, to render camera feed without distortion, 16:9 ratio
// should be used fo iOS devices and 4:3 ratio should be used for android
// devices.
//
// This might not cover all cases.
const CAM_PREVIEW_WIDTH = window.width;
const CAM_PREVIEW_HEIGHT = CAM_PREVIEW_WIDTH / (IS_IOS ? 9 / 16 : 3 / 4);

// The score threshold for pose detection results.
const MIN_KEYPOINT_SCORE = 0.3;

// The size of the resized output from TensorCamera.
//
// For movenet, the size here doesn't matter too much because the model will
// preprocess the input (crop, resize, etc). For best result, use the size that
// doesn't distort the image.
const OUTPUT_TENSOR_WIDTH = 180;
const OUTPUT_TENSOR_HEIGHT = OUTPUT_TENSOR_WIDTH / (IS_IOS ? 9 / 16 : 3 / 4);

// Whether to auto-render TensorCamera preview.
const AUTO_RENDER = false;

// Whether to load model from app bundle (true) or through network (false).
const LOAD_MODEL_FROM_BUNDLE = true;
const estimationConfig = { flipHorizontal: true };
export default function PoseDetector({ navigation, route }) {
  const { data } = route.params;
  const cameraRef = useRef(null);
  const [tfReady, setTfReady] = useState(true);
  const [model, setModel] = useState<posedetection.PoseDetector>();
  const [poses, setPoses] = useState<posedetection.Pose[]>();
  const [fps, setFps] = useState(0);
  // const [orientation, setOrientation] = useState<ScreenOrientation.Orientation>();
  const [cameraType, setCameraType] = useState<CameraType>(
    Camera.Constants.Type.front
  );
  const [poseDataByAngle, setPoseDataByAngle] = useState("");
  // Use `useRef` so that changing it won't trigger a re-render.
  // - null: unset (initial value).
  // - 0: animation frame/loop has been canceled.
  // - >0: animation frame has been scheduled.
  const rafId = useRef<number | null>(null);

  useEffect(() => {
    async function prepare() {
      rafId.current = null;

      // Set initial orientation.
      /* const curOrientation = await ScreenOrientation.getOrientationAsync();
            setOrientation(curOrientation);

            // Listens to orientation change.
            ScreenOrientation.addOrientationChangeListener((event) => {
                setOrientation(event.orientationInfo.orientation);
            }); */
      // Camera permission.
      await Camera.requestCameraPermissionsAsync();

      // Wait for tfjs to initialize the backend.
      await tf.ready();
      // Load movenet model.
      // https://github.com/tensorflow/tfjs-models/tree/master/pose-detection
      const movenetModelConfig: posedetection.MoveNetModelConfig = {
        modelType: posedetection.movenet.modelType.SINGLEPOSE_LIGHTNING,
        enableSmoothing: true,
        minPoseScore: 0.3,
      };
      if (LOAD_MODEL_FROM_BUNDLE) {
        const modelJson = require("../offline_model/model.json");
        const modelWeights1 = require("../offline_model/group1Shard1of2.bin");
        const modelWeights2 = require("../offline_model/group1Shard2of2.bin");
        movenetModelConfig.modelUrl = `${bundleResourceIO(modelJson, [
          modelWeights1,
          modelWeights2,
        ])}`;
      }
      setModel(
        await posedetection.createDetector(
          posedetection.SupportedModels.MoveNet,
          movenetModelConfig
        )
      );
      // Ready!
      setTfReady(true);
    }
    prepare();
  }, []);

  useEffect(() => {
    // Called when the app is unmounted.
    return () => {
      if (rafId.current != null && rafId.current !== 0) {
        cancelAnimationFrame(rafId.current);
        rafId.current = 0;
      }
    };
  }, []);

  const calculate_joints_angle = (
    first_landmark,
    second_landmark,
    third_landmark
  ) => {
    if (
      typeof first_landmark &&
      typeof second_landmark &&
      typeof third_landmark !== "undefined"
    ) {
      const x1 = first_landmark.x;
      const y1 = first_landmark.y;
      const x2 = second_landmark.x;
      const y2 = second_landmark.y;
      const x3 = third_landmark.x;
      const y3 = third_landmark.y;

      const angle_in_radians =
        Math.atan2(y3 - y2, x3 - x2) - Math.atan2(y1 - y2, x1 - x2);
      let angle = Math.abs(angle_in_radians * (180 / Math.PI));
      if (angle > 180) {
        angle = 360 - angle;
      }
      return angle.toFixed(0);
    } else return 0;
  };

  const handleCameraStream = async (
    images: IterableIterator<tf.Tensor3D>,
    updatePreview: () => void,
    gl: ExpoWebGLRenderingContext
  ) => {
    const loop = async () => {
      // Get the tensor and run pose detection.
      const imageTensor = images.next().value as tf.Tensor3D;
      const startTs = Date.now();
      const poses = await model!.estimatePoses(
        imageTensor,
        estimationConfig,
        Date.now()
      );
      const latency = Date.now() - startTs;
      setFps(Math.floor(1000 / latency));
      setPoses(poses);
      tf.dispose([imageTensor]);

      // if (rafId.current === 0) {
      //   return;
      // }
      // Render camera preview manually when autorender=false.
      if (!AUTO_RENDER) {
        updatePreview();
        gl.endFrameEXP();
      }

      rafId.current = requestAnimationFrame(loop);
      console.log("rafId.current ", rafId.current);
    };
    loop();
  };

  const analyzeKeypoints = () => {
    let leftShoulder;
    let rightShoulder;
    let leftElbow;
    let rightElbow;
    let leftWrist;
    let rightWrist;
    let leftHip;
    let rightHip;
    let leftKnee;
    let rightKnee;
    let leftAnkle;
    let rightAnkle;

    // let information = [];

    if (poses != null && poses.length > 0) {
      const keypoints_set = poses[0].keypoints;

      leftShoulder = keypoints_set.find((k) => k.name === "left_shoulder");
      rightShoulder = keypoints_set.find((k) => k.name === "right_shoulder");

      leftElbow = keypoints_set.find((k) => k.name === "left_elbow");
      rightElbow = keypoints_set.find((k) => k.name === "right_elbow");

      leftWrist = keypoints_set.find((k) => k.name === "left_wrist");
      rightWrist = keypoints_set.find((k) => k.name === "right_wrist");

      leftHip = keypoints_set.find((k) => k.name === "left_hip");
      rightHip = keypoints_set.find((k) => k.name === "right_hip");

      leftKnee = keypoints_set.find((k) => k.name === "left_knee");
      rightKnee = keypoints_set.find((k) => k.name === "right_knee");

      leftAnkle = keypoints_set.find((k) => k.name === "left_ankle");
      rightAnkle = keypoints_set.find((k) => k.name === "right_ankle");
    }

    const left_elbow_angle = calculate_joints_angle(
      leftShoulder,
      leftElbow,
      leftWrist
    );
    const right_elbow_angle = calculate_joints_angle(
      rightShoulder,
      rightElbow,
      rightWrist
    );
    const left_shoulder_angle = calculate_joints_angle(
      leftElbow,
      leftShoulder,
      leftHip
    );
    const right_shoulder_angle = calculate_joints_angle(
      rightHip,
      rightShoulder,
      rightElbow
    );
    const left_knee_angle = calculate_joints_angle(
      leftHip,
      leftKnee,
      leftAnkle
    );
    const right_knee_angle = calculate_joints_angle(
      rightHip,
      rightKnee,
      rightAnkle
    );

    setPoseDataByAngle(
      `L Elbow: ${left_elbow_angle}, R Elbow: ${right_elbow_angle}, 
      L Shoulder: ${left_shoulder_angle}, R Shoulder: ${right_shoulder_angle}, 
      L Knee: ${left_knee_angle}, R Knee: ${right_knee_angle}`
    );
  };

  const renderPose = () => {
    if (poses != null && poses.length > 0) {
      const keypoints_set = poses[0].keypoints;
      const keypoints_for_render = keypoints_set
        .filter((k) => (k.score ?? 0) > MIN_KEYPOINT_SCORE)
        .map((k) => {
          // Flip horizontally on android or when using back camera on iOS.
          const flipX = IS_ANDROID || cameraType === Camera.Constants.Type.back;
          const x = flipX ? getOutputTensorWidth() - k.x : k.x;
          const y = k.y;
          const cx = (x / getOutputTensorWidth()) * CAM_PREVIEW_WIDTH;
          const cy = (y / getOutputTensorHeight()) * CAM_PREVIEW_HEIGHT;

          return (
            <Circle
              key={`skeletonkp_${k.name}`}
              cx={cx}
              cy={cy}
              r="4"
              strokeWidth="2"
              fill="#00AA00"
              stroke="white"
            />
          );
        });

      return <Svg style={styles.svg}>{keypoints_for_render}</Svg>;
    }
  };

  const renderFps = () => {
    return (
      <View style={styles.fpsContainer}>
        <Text>FPS: {fps}</Text>
      </View>
    );
  };

  const handleSwitchCameraType = () => {
    if (cameraType === Camera.Constants.Type.front) {
      setCameraType(Camera.Constants.Type.back);
    } else {
      setCameraType(Camera.Constants.Type.front);
    }
  };

  const renderCameraTypeSwitcher = () => {
    return (
      <View
        style={styles.cameraTypeSwitcher}
        onTouchEnd={handleSwitchCameraType}
      >
        <Text>
          Switch to
          {cameraType === Camera.Constants.Type.front ? "back" : "front"} camera
        </Text>
      </View>
    );
  };

  /*   const isPortrait = () => {
        return orientation === ScreenOrientation.Orientation.PORTRAIT_UP || orientation === ScreenOrientation.Orientation.PORTRAIT_DOWN;
    }; */

  const getOutputTensorWidth = () => {
    // On iOS landscape mode, switch width and height of the output tensor to
    // get better result. Without this, the image stored in the output tensor
    // would be stretched too much.
    //
    // Same for getOutputTensorHeight below.
    //return isPortrait() || IS_ANDROID ? OUTPUT_TENSOR_WIDTH : OUTPUT_TENSOR_HEIGHT;
    return OUTPUT_TENSOR_HEIGHT;
  };

  const getOutputTensorHeight = () => {
    //return isPortrait() || IS_ANDROID ? OUTPUT_TENSOR_HEIGHT : OUTPUT_TENSOR_WIDTH;
    return OUTPUT_TENSOR_WIDTH;
  };

  /* const getTextureRotationAngleInDegrees = () => {
        // On Android, the camera texture will rotate behind the scene as the phone
        // changes orientation, so we don't need to rotate it in TensorCamera.
        if (IS_ANDROID) {
            return 0;
        }

        // For iOS, the camera texture won't rotate automatically. Calculate the
        // rotation angles here which will be passed to TensorCamera to rotate it
        // internally.
        switch (orientation) {
            // Not supported on iOS as of 11/2021, but add it here just in case.
            case ScreenOrientation.Orientation.PORTRAIT_DOWN:
                return 180;
            case ScreenOrientation.Orientation.LANDSCAPE_LEFT:
                return cameraType === Camera.Constants.Type.front ? 270 : 90;
            case ScreenOrientation.Orientation.LANDSCAPE_RIGHT:
                return cameraType === Camera.Constants.Type.front ? 90 : 270;
            default:
                return 0;
        }
    }; */

  if (!tfReady) {
    console.log("TF loading");
    return (
      <View style={styles.loadingMsg}>
        <View style={{ flex: 1, justifyContent: "center" }}>
          <Text style={{ fontWeight: "bold" }}>
            Loading necessary resources...
          </Text>
        </View>
        {/* <View style={{ flex: 1 }}>
          <ActivityIndicator size="small" />
        </View> */}
      </View>
    );
  } else {
    console.log(poseDataByAngle);
    analyzeKeypoints();
    return (
      // Note that you don't need to specify `cameraTextureWidth` and
      // `cameraTextureHeight` prop in `TensorCamera` below.
      <View>
        <View style={styles.buttonCloseCamera}>
          <MaterialCommunityIcons
            name="arrow-left"
            size={24}
            color="#fff"
            onPress={() => navigation.navigate("Index")}
          />
          <Text
            style={{
              color: "#fff",
              fontSize: 20,
              fontWeight: "bold",
              alignItems: "center",
              paddingLeft: 100,
            }}
          >
            {data}
          </Text>
        </View>
        <View style={styles.containerPortrait}>
          <TensorCamera
            ref={cameraRef}
            style={styles.camera}
            autorender={AUTO_RENDER}
            type={cameraType}
            // tensor related props
            resizeWidth={getOutputTensorWidth()}
            resizeHeight={getOutputTensorHeight()}
            resizeDepth={3}
            rotation={0}
            onReady={handleCameraStream}
            useCustomShadersToResize={false}
            cameraTextureWidth={window.width}
            cameraTextureHeight={window.height}
          />
          {renderPose()}
          {renderFps()}
          {renderCameraTypeSwitcher()}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerPortrait: {
    position: "relative",
    width: CAM_PREVIEW_WIDTH,
    height: CAM_PREVIEW_HEIGHT,
    marginTop: Dimensions.get("screen").height / 2 - CAM_PREVIEW_HEIGHT / 2,
  },
  containerLandscape: {
    position: "relative",
    width: CAM_PREVIEW_HEIGHT,
    height: CAM_PREVIEW_WIDTH,
    marginLeft: Dimensions.get("screen").height / 2 - CAM_PREVIEW_HEIGHT / 2,
  },
  loadingMsg: {
    position: "absolute",
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  camera: {
    width: "100%",
    height: "100%",
    zIndex: 1,
  },
  svg: {
    width: "100%",
    height: "100%",
    position: "absolute",
    zIndex: 30,
  },
  fpsContainer: {
    position: "absolute",
    top: 10,
    left: 10,
    width: 80,
    alignItems: "center",
    backgroundColor: "rgba(255, 255, 255, .7)",
    borderRadius: 2,
    padding: 8,
    zIndex: 20,
  },
  cameraTypeSwitcher: {
    position: "absolute",
    top: 10,
    right: 10,
    width: 180,
    alignItems: "center",
    backgroundColor: "rgba(255, 255, 255, .7)",
    borderRadius: 2,
    padding: 8,
    zIndex: 20,
  },
  angleDisplayer: {
    position: "absolute",
    top: 100,
    right: 10,
    width: 180,
    alignItems: "center",
    backgroundColor: "rgba(255, 255, 255, .7)",
    borderRadius: 2,
    padding: 8,
    zIndex: 20,
  },
  buttonCloseCamera: {
    paddingTop: window.height * 0.06,
    paddingLeft: window.width * 0.06,
    paddingBottom: window.width * 0.03,
    backgroundColor: "#005DB4",
    flexDirection: "row",
    // alignItems: "center",
  },
});
