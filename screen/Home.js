import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { Ionicons } from "react-native-vector-icons";
import Workout from "./WorkoutSelectPage";
import Statistic from "./StatisticScreen";
import Record from "./RecordScreen";
import Profile from "./Profile";
const Tab = createBottomTabNavigator();

export default function Home({ navigation }) {
  return (
    <Tab.Navigator
      initialRouteName="Workout"
      screenOptions={{
        tabBarActiveTintColor: "#0466C8",
        headerShown: false,
      }}
    >
      <Tab.Screen
        name="Workout"
        component={Workout}
        listeners={{
          tabPress: () => navigation.setOptions({ title: "Workout" }),
        }}
        options={{
          tabBarLabel: "Workout",
          tabBarIcon: ({ color, size }) => (
            <Ionicons name="flame-outline" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Statistic"
        component={Statistic}
        listeners={{
          tabPress: () => navigation.setOptions({ title: "Statistic" }),
        }}
        options={{
          tabBarLabel: "Statistic",
          tabBarIcon: ({ color, size }) => (
            <Ionicons name="analytics-outline" color={color} size={size} />
          ),
        }}
      />

      <Tab.Screen
        name="Profile"
        component={Profile}
        listeners={{
          tabPress: () => navigation.setOptions({ title: "Profile" }),
        }}
        options={{
          tabBarLabel: "Profile",
          tabBarIcon: ({ color, size }) => (
            <Ionicons name="person-outline" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Record"
        component={Record}
        listeners={{
          tabPress: () => navigation.setOptions({ title: "Record" }),
        }}
        options={{
          tabBarLabel: "Record",
          tabBarIcon: ({ color, size }) => (
            <Ionicons name="bookmarks-outline" color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}
