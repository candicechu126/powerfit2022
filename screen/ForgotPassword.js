import React, { useState } from "react";
import {
  Text,
  KeyboardAvoidingView,
  TouchableOpacity,
  TextInput,
  View,
  StyleSheet,
  Dimensions,
  Image,
  Button,
} from "react-native";

const window = Dimensions.get("window");
const screen = Dimensions.get("screen");

const ForgotPassword = ({ navigation }) => {
  const [email, setEmail] = useState();
  const [isInputEmail, setIsInputEmail] = useState(false);
  const handleEmail = () => setIsInputEmail(true);
  const handleBlurEmail = () => setIsInputEmail(false);
  const handleSignup = () => navigation.navigate("SignupScreen");
  const handleLogin = () => navigation.navigate("LoginScreen");
  const handleReset = () => navigation.navigate("");

  return (
    <View style={styles.container}>
      <View
        style={{
          backgroundColor: "#0466C8",
          width: screen.width,
          flex: 1,
        }}
      />
      <View
        style={{
          width: screen.width,
          backgroundColor: "#E6F0FA",
          flex: 1,
        }}
      />
      <View
        style={{
          position: "absolute",
          justifyContent: "center",
          alignItems: "center",
          paddingTop: 30,
          backgroundColor: "#fff",
          borderRadius: 6,
          width: "85%",
          height: "60%",
        }}
      >
        <Image
          source={require("../assets/component/forgot_password.png")}
          style={styles.img}
        />
        <Text style={styles.resetTitle}>RESET PASSWORD</Text>
        <KeyboardAvoidingView
          behavior={Platform.OS === "ios" ? "padding" : "height"}
        >
          <TextInput
            style={styles.input}
            onFocus={handleEmail}
            onBlur={handleBlurEmail}
            placeholderTextColor={isInputEmail ? "#C9E3EE" : "#7EB6DF"}
            placeholder="Email"
            value={email}
            autoCapitalize="none"
            onChangeText={(text) => setEmail(text)}
            clearButtonMode="while-editing"
          />

          <TouchableOpacity onPress={handleReset} style={styles.resetButton}>
            <Text style={styles.resetText}>Reset</Text>
          </TouchableOpacity>

          <View style={styles.questionContainer}>
            <Text style={styles.questionText}>Dont't have an account yet?</Text>
            <TouchableOpacity onPress={handleSignup}>
              <Text style={styles.textBtn}>Sign Up</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.questionContainer}>
            <Text style={styles.questionText}>Already have an account?</Text>
            <TouchableOpacity onPress={handleLogin}>
              <Text style={styles.textBtn}>Sign In</Text>
            </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>
      </View>
    </View>
  );
};

export default ForgotPassword;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  resetTitle: {
    color: "#0466C8",
    fontWeight: "bold",
    fontSize: 24,
    paddingTop: screen.height * 0.01,
    paddingBottom: screen.height * 0.03,
  },
  img: {
    width: "70%",
    height: "30%",
  },
  input: {
    margin: 15,
    width: screen.width * 0.7,
    padding: 12,
    borderRadius: 25,
    fontSize: 16,
    alignSelf: "center",
    backgroundColor: "#fff",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
  },
  resetButton: {
    width: screen.width * 0.7,
    backgroundColor: "#0466C8",
    marginTop: 12,
    marginBottom: 25,
    maxHeight: 80,
    borderRadius: 25,
    padding: 10,
    alignItems: "center",
    alignSelf: "center",
  },
  resetText: {
    color: "#fff",
    fontSize: 20,
    fontWeight: "bold",
  },
  textBtn: {
    color: "#0466C8",
    fontWeight: "bold",
    fontSize: 16,
  },
  questionText: {
    color: "#616161",
    fontSize: 14,
  },
  questionContainer: {
    color: "#616161",
    flexDirection: "row",
    paddingTop: 20,
    alignSelf: "center",
  },
});
